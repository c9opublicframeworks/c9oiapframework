# C9OIAPService
C9OIAPService framework is provided by Cloud9 Online LLC to manage In-App Purchases.

## Requirements
- Xcode 12.0 or later
- iOS 11.0 or later
- Swift 5.0 or later

## Installation
### Cocoapods
1. This framework is available through [CocoaPods](http://cocoapods.org). 
2. Include C9OIAPService in your `Pod File`:

```
pod 'C9OIAPService'
```
   
## Usage

1. Import the C9OIAPService header in the application delegate.
```swift
import C9OIAPService
```
2. Add following code in your appDelegate's application:didFinishLaunchingWithOptions: method. This will setup and initialize the framework and fetch In-App products. Pass your In-App product ids as array.
```swift
C9OIAPService.shared.initialize(inAppIds: [
    "com.myapp.monthlysubscription"
])
```
3. Use following code to get all In-App products.
```swift
import C9OIAPService
let products = C9OIAPService.shared.products
```    
4. Use following method to initiate a product purchase. **startPurchaseFor** takes **SKProduct** object returned in previous step.

```swift
C9OIAPService.shared.startPurchaseFor(product: <SKProduct>)
```
    
## Observers
To Subscribe
```swift
let observerClient = C9OIAPService.shared.addPurchaseObserver(observer: self)
```

After subscribing for observer, you can receive purchase updates.
```swift
extension IAPViewController: C9OIAPServicePurchaseObserver {

    func didFetchProducts(products: [SKProduct]) {
    }

    func didFailedPurchaseWithError(error: String) {
    }

    func didFailValidatingPurchase() {
    }

    func didPurchaseProduct(product: SKProduct) {
    }

}
```

To Unsubscribe
```swift
C9OIAPService.shared.removeObserver(client: observerClient)
```    
