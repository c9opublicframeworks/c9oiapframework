Pod::Spec.new do |spec|

  spec.name                     = "C9OIAPService"
  spec.version                  = "0.0.2"
  spec.summary                  = "C9OIAPService framework is provided by Cloud9 Online LLC to manage In-App Purchases."
  spec.description              = "C9OIAPService contains the functionality to handle In-App Purchases. User can fetch list of available in app purchases and also listen for purchase process through a set of observers."
  spec.homepage                 = "https://gitlab.com/c9opublicframeworks/c9oiapframework"
  spec.license                  = { :type => 'MIT', :text => <<-LICENSE
                                        Copyright © 2021 Cloud9 Online LLC.
                                    LICENSE
                                  }
  spec.author                   = { "Cloud9 Online" => "cloud9online@gmail.com" }
  spec.ios.deployment_target    = "11.0"
  spec.source                   = { :git => "https://gitlab.com/c9opublicframeworks/c9oiapframework.git", :tag => "#{spec.version}" }
  spec.vendored_frameworks      = "C9OIAPService.framework"
  spec.swift_versions           = ['5.0']
  
  spec.dependency                 "C9OCore"
  spec.weak_frameworks          = 'Foundation', 'SystemConfiguration'
  
  spec.user_target_xcconfig     = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  spec.pod_target_xcconfig      = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  spec.pod_target_xcconfig      = { 'VALID_ARCHS' => 'x86_64 armv7 arm64' }

end
