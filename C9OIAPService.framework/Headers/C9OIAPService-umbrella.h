#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "C9OIAPService.h"

FOUNDATION_EXPORT double C9OIAPServiceVersionNumber;
FOUNDATION_EXPORT const unsigned char C9OIAPServiceVersionString[];

